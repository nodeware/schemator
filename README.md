Schemator
=========

Now is easy to build, validate and sanitize your own object schema.
Schemator is independent of any database, this means that you can
use it for any back-end operations, including database ones.
Plus: it is 100% asynchronous.

#### How to use

```javascript
// Require it and instantiate a new schema
var schemator = require('schemator');
var schema = new schemator();

// Now all you need to do is to specify what
// fields must be validated, like this:
schema.validate('name', {
    required : 'You must enter your name.',
    betweenLength : [10, 80, 'Your name must have between 10 and 80 characters.']
});

// Nested keys can be accessed using dot notation
schema.validate('phone.area', {
    length : [3, 'Your phone area code must have 3 digits.'],
    optional : true //optional must come last
});

schema.validate('phone.number', {
    betweenLength : [8, 12, 'Your phone number must have between 8 and 12 digits.'],
    optional : true
});

// You can also pass more than one field at a time
schema.validate({
    'password' : {
        required : 'You must type your password.'
    },
    'password_check' : {
        equalTo : ['password', 'The passwords aren\'t equals.']
    }
});

// Now it's time to validate some object, this is the object
// that we'll validade:
var obj = {
    name : 'Albert Einstein',
    phone : {
        area : '051',
        number : '1234'
    },
    password : 'ilovetheory',
    password_check : 'iloveOPS',
    this_field : 'Won\'t appear in the validated object'
};

// Time to validade! All you need to pass is the object
// that needs validation and a callback.
schema.run(obj, function(err, invalid, data){
    // Do not continue if some server error occurs
    if(err)
        throw err;

    // Inform the user if there is invalid data
    if(invalid)
        req.send('Your data is invalid')

    // Insert the new user in your database if no error
    if(data)
        db.user.insert(data)
});

```

#### Examples

Invalid object returned in the callback:

```javascript
[{
    path: 'phone.number',
    errors: ['Your phone number must have between 8 and 12 digits.']
}, {
    path: 'password_check',
    errors: ['The passwords aren\'t equals.']
}]
```

Validated object returned in the callback:

```javascript
{
    name: 'Albert Einstein',
    phone: {
        area: '051',
        number: '123445678'
    },
    password: 'ilovetheory',
    password_check: 'ilovetheory'
}
```

#### Observations

* At this time, fields that aren't listed in the validation won't
appear in the final result. Maybe a configuration will be added
to allow unknown fields.

* Optional fields that aren't present in the object being validated
will appear in the final result with an empty string as it's value.

#### ToDo

* In-code documentation
* Generate documentation
* Sanitization
* More validators
* Tell in this readme about all validators/sanitizators
* Cleanup the code.

#### License

The MIT License (MIT)

Copyright (c) 2013 Nodeware

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.


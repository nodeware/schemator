var async = require('async');
var dot = require('dot-component');

var rules = {
    'required' : function(value, options, callback){
        if(value === undefined)
            callback(null, options);
        else
            callback();
    },
    'minLength' : function(value, options, callback){
        if(typeof value === 'number')
            value = value.toString();

        if(value === undefined || typeof value.length === undefined || value.length < options[0])
            callbacck(null, options[1]);
        else
            callback();
    },
    'maxLength' : function(value, options, callback){
        if(typeof value === 'number')
            value = value.toString();

        if(value === undefined || typeof value.length === undefined || value.length > options[0])
            callbacck(null, options[1]);
        else
            callback();
    },
    'email' : function(value, options, callback){
        var regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(!regex.test(value))
            callback(null, options);
        else
            callback();
    },
    'length' : function(value, options, callback){
        if(typeof value === 'number')
            value = value.toString();

        if(value === undefined || typeof value.length === 'undefined' || value.length !== options[0])
            callback(null, options[1]);
        else
            callback();
    },
    'betweenLength' : function(value, options, callback){
        if(typeof value === 'number')
            value = value.toString();

        if(value === undefined || typeof value.length === 'undefined'|| value.length < options[0] || value.length > options[1])
            callback(null, options[2]);
        else
            callback();
    },
    'inList': function(value, options, callback){
        if(value === undefined || options[0].indexOf(value) === -1)
            callback(null, options[1]);
        else
            callback();
    },
    'equalTo' : function(value, options, callback){
        if(value !== dot.get(this.obj, options[0]))
            callback(null, options[1]);
        else
            callback();
    },
    'optional' : function(value, options, callback){
        if(!value || !/\S/g.test(value))
            callback(null, null, true);
        else
            callback();
    }
};

var sanitizators = {

};

var Schemator = function(){
    var that = this;

    var paths = {
        validate : {},
        sanitize : {}
    };

    this.validate = function(path, rules){
        if(path instanceof Object)
            return Object.keys(path).forEach(function(key){
                that.validate(key, path[key]);
            });

        if(!paths.validate[path])
            paths.validate[path] = {};

        Object.keys(rules).forEach(function(key){
            paths.validate[path][key] = rules[key];
        });
    };


    this.sanitize = function(path, modules){

    };

    this.run = function(obj, callback){
        var ret = {};
        var errors = [];

        var setError = function(path, message){
            var index = -1;

            errors.forEach(function(err, idx){
                if(err.path && err.path === path)
                    index = idx;
            });

            if(index !== -1)
                errors[index].errors.push(message);
            else
                errors.push({
                    path : path,
                    errors : [message]
                });
        };

        var clearError = function(path){
            errors.forEach(function(err, idx){
                if(err.path && err.path === path)
                    errors.splice(idx, 1);
            });
        };

        async.eachSeries(Object.keys(paths.validate), function(path, cb1){
            var passed = true;
            var optional = false;
            var value = dot.get(obj, path);

            async.eachSeries(Object.keys(paths.validate[path]), function(validator, cb2){
                rules[validator].apply({obj : obj}, [value, paths.validate[path][validator], function(err, inv, clear){
                    if(inv){
                        setError(path, inv);
                        passed = false;
                    }

                    if(clear){
                        clearError(path);
                        optional = true;
                    }

                    cb2(err, inv);
                }]);
            }, function(err, inv){
                if(passed)
                    dot.set(ret, path, value);

                if(!passed && optional)
                    dot.set(ret, path, '');

                cb1(err);
            });
        }, function(err){
            callback(err, !errors.length ? null : errors, !errors.length ? ret : null);
        });
    };

};

module.exports = Schemator;

// Require it and instantiate a new schema
var schemator = require('./app');
var schema = new schemator();

// Now all you need to do is to specify what
// fields must be validated, like this:
schema.validate('name', {
    required : 'You must enter your name.',
    betweenLength : [10, 80, 'Your name must have between 10 and 80 characters.']
});

// Nested keys can be accessed using dot notation
schema.validate('phone.area', {
    length : [3, 'Your phone area code must have 3 digits.'],
    optional : true //optional must come last
});

schema.validate('phone.number', {
    betweenLength : [8, 12, 'Your phone number must have between 8 and 12 digits.'],
    optional : true
});

// You can also pass more than one field at a time
schema.validate({
    'password' : {
        required : 'You must type your password.'
    },
    'password_check' : {
        equalTo : ['password', 'The passwords aren\'t equals.']
    }
});

// Now it's time to validate some object, this is the object
// that we'll validade:

var obj = {
    name : 'Albert Einstein',
    phone : {
        area : '051',
        number : '123445678'
    },
    password : 'ilovetheory',
    password_check : 'ilovetheory',
    this_field : 'Won\'t appear in the validated object'
};

// Time to validade!
schema.run(obj, function(err, invalid, data){
    // Do not continue if some server error occurs
    console.log(data);
});
